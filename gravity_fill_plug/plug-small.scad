$fn=$preview ? 16 : 64;

include <tsmthread4.scadlib>;

// these can be adjusted to change the design (values in inches)

// height of threaded portion
plug_height = 1/2;
// cannot be larger or interferes with threads
base_width = 1.44; 

// height of handle
handle_height = 1.1;

// width of handle
handle_width = 1+2/16;

// thickness (diameter) of handle
handle_thick = 6/16;

// lower the handle into the base
handle_drop = 7/32;

// don't ask
fudge = 1/16;

// sets angle at top of removed portion under cap
cone_drop = 3/16;

// adds an cylinder below threads to help alignment
align_width = 1.475;
align_height = 3/32;

// don't add hole for valve unless used in other file
valve_enable = 0;


imperial() {
    // everything is in inches

    // alignment helper - extend tube below threads
    difference() {
        cylinder(d=align_width, h=align_height);
        cylinder(d=1.25, h=align_height);
    }
    
    // move rest of part up above alignment
    translate([0, 0, align_height]) {
        difference() {
            union() {
                difference() {
                    // NPT 1-1/4" threads
                    thread_npt(DMAJ=1.660+$OD_COMP, PITCH=1/11.5, L=plug_height);
                    // put a hole in the plug from the bottom to reduce print time
                    cylinder(d=1.25, h=plug_height - fudge - cone_drop);
                    // add a cone to the top of the hole (printability)
                    translate([0, 0, plug_height - fudge - cone_drop])
                        cylinder(d1=1.25, d2=0, h=cone_drop);
                    // trim off the bottom edge of the threads, easier to insert
                    difference() {
                        cylinder(d=2, h=1/4);
                        cylinder(d1=base_width, d2=2, h=1/4);
                    }
                }
                // create a base for the handle on top of the plug
                hull() {
                    // match the plug top
                    translate([0, 0, plug_height-fudge])
                        cylinder(d=base_width, h=0.05);
                    // cube parallel with handle
                    base_thick = handle_thick - fudge*2;
                    translate([-base_width/2, -base_thick/2, plug_height + fudge])
                        cube([base_width, base_thick, handle_thick/2]);
                }
            }
            if (valve_enable) {
                translate([0, -valve_forward, valve_raise]) rotate([valve_rotate, 0, 0]) {
                    cylinder(d=valve_diameter, h=valve_length);
                }
            }
        }
	if (valve_enable) {
            translate([0, -valve_forward, valve_raise]) rotate([valve_rotate, 0, 0]) {
                difference() {
                    cylinder(d=valve_tube, h=valve_length);
                    cylinder(d=valve_diameter, h=valve_length);
                }
            }
        }
        // construct handle as series of streched spheres
        translate([0, 0, plug_height - fudge+handle_thick/2])
        handle(handle_width, handle_thick, handle_height, handle_drop);
    }
}

module handle(width, thick, height, drop) {
    points = [
    [ 0, 0, 0],
    [ width/2, 0, drop],
    [ width/2, 0, height-drop],
    [ 0, 0, height],
    [ -width/2, 0, height-drop],
    [ -width/2, 0, drop],
    [ 0, 0, 0],
    ];
    for (i = [0:len(points)-2]) {
        hull() {
            translate(points[i]) sphere(d=thick);
            translate(points[i+1]) sphere(d=thick);
        }
    }
}

