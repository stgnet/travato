$fn=$preview ? 16 : 64;

include <tsmthread4.scadlib>;

// these can be adjusted to change the design
plug_height = 1/2;
base_width = 1.44; // cannot be larger or interferes with threads
handle_height = 1.5;
handle_width = 2+1/2;
handle_thick = 6/16;
handle_drop = 10/32;
fudge = 1/16;
cone_drop = 3/16;

align_width = 1.475;
align_height = 1/8;

#translate([0, -5, 10]) rotate([10, 0, 0]) {
    cylinder(d=7, h=40);
    translate([0, 0, 16]) cylinder(d=19, h=8);
}

imperial() {
    difference() {
        cylinder(d=align_width, h=align_height);
        cylinder(d=1.25, h=align_height);
    }
    translate([0, 0, align_height]) {
        
        // everything is in inches
        difference() {
            // NPT 1-1/4" threads
            thread_npt(DMAJ=1.660+$OD_COMP, PITCH=1/11.5, L=plug_height);
            // put a hole in the plug from the bottom to reduce print time
            cylinder(d=1.25, h=plug_height - fudge - cone_drop);
            // add a cone to the top of the hole (printability)
            translate([0, 0, plug_height - fudge - cone_drop])
                cylinder(d1=1.25, d2=0, h=cone_drop);
            // trim off the bottom edge of the threads, easier to insert
            difference() {
                cylinder(d=2, h=1/4);
                cylinder(d1=base_width, d2=2, h=1/4);
            }
        }
        // create a base for the handle on top of the plug
        hull() {
            // match the plug top
            translate([0, 0, plug_height-fudge])
                cylinder(d=base_width, h=0.1);
            // cube parallel with handle
            translate([-base_width/2, -handle_thick/2, plug_height - fudge])
                cube([base_width, handle_thick, handle_thick/2]);
        }
        // construct handle as series of streched spheres
        translate([0, 0, plug_height - fudge+handle_thick/2])
            handle(handle_width, handle_thick, handle_height, handle_drop);
    }
}

module handle(width, thick, height, drop) {
    points = [
        [ 0, 0, 0],
        [ width/2, 0, drop],
        [ width/2, 0, height-drop],
        [ 0, 0, height],
        [ -width/2, 0, height-drop],
        [ -width/2, 0, drop],
        [ 0, 0, 0],
    ];
    for (i = [0:len(points)-2]) {
        hull() {
            translate(points[i]) sphere(d=thick);
            translate(points[i+1]) sphere(d=thick);
        }
    }
}
