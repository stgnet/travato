$fn=$preview ? 16 : 64;

hole_diameter = 7;
hole_length = 11;

cube=16;

difference() {
	translate([-cube/2, -cube/2, 0])
		cube([cube*6, cube, hole_length]);
    for (i = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8])
        translate([100*i, 0, 0])
            cylinder(d=hole_diameter+i, h=hole_length);
}

