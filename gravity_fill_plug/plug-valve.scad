include <plug-small.scad>;

// this adds a hole for a one-way valve to allow air into the tank
// which has been shown to eliminate an overflow siphon problem when filling
//
// valve is: Cole-Parmer Nylon Miniature Check Valve
//           1/4" hose barb, fluorosilicone diaphragm
//           Model Number	9855302
//           UNSPSC Code	41120000

// enable a hole for the valve
valve_enable=1;

// tip the valve forward so it doesn't intefere with handle
valve_rotate = 14;

// move it forward of center
valve_forward = 7 / 25.4;

// raise it up to clear top
valve_raise = 8.65 / 25.4;

// experimentally determined to fit valve tightly
valve_diameter = 7.2 / 25.4; 

// length of hole for valve to fit
valve_length = 11 / 25.4;

// outer diameter of tube around hole
valve_tube = 10 / 25.4;

// actual code implementing hole is in plug-small.scad