$fn=$preview ? 16 : 64;

rounding = 2.5;

// section 1
bolt_head_depth = 5.4;
bolt_head_diameter = 13;
section_1_outer_diameter = 17;

module section_1_hole() {
    cylinder(d=bolt_head_diameter, h=bolt_head_depth, center=false);
}

// section 2
bolt_hole_depth = 3;
bolt_hole_diameter = 6.5;
module section_2_hole() {
    cylinder(d=bolt_hole_diameter, h=bolt_hole_depth, center=false);
}

// section 3
square_post_width = 18.2;
square_post_depth = 11.5;
section_3_outer_diameter = 29;

module section_3_hole() {
    rotate([0, 0, 45]) 
        translate([-square_post_width/2, -square_post_width/2, 0])
            cube([square_post_width, square_post_width, square_post_depth]);
}

// section 4
rim_inside_diameter_1 = 25;
rim_inside_diameter_2 = 31;
rim_outside_diameter_1 = 29;
rim_outside_diameter_2 = 34.9;

handle_height = 120;
handle_width = 3 * 25.4;
handle_thick = 5.5;

height = square_post_depth + bolt_hole_depth + bolt_head_depth;

module rcube(size, rr) {
    minkowski() {
        translate([rr, rr, rr])
            cube([size[0]-2*rr, size[1]-2*rr, size[2]-2*rr]);
        sphere(r=rr);
    }
}
// rounded cylinder by minkowski with sphere
module rcylinder(h, d, center, rr) {
    minkowski() {
        translate([0, 0, center?0:rr]) cylinder(h=h-2*rr, d=d-2*rr, center=center);
        sphere(r=rr);
    }
}

support_bar_width = 20;

pull_width = 1.5 * 25.4;
pull_thick = 1;

/*
            translate([handle_height-1, 0, rounding])
                minkowski() {
                    cylinder(d=1, h=height-rounding*2, center=false);
                    sphere(r=rounding);
                }
*/
module pull() {
    translate([pull_width/2-pull_thick-rounding, 0, rounding]) {
        minkowski() {
            intersection() {
                difference() {
                    cylinder(h=handle_width-2*rounding, d=pull_width, center=false);
                    cylinder(h=handle_width-2*rounding, d=pull_width-pull_thick*2, center=false);
                }
                translate([-pull_width, -pull_width/2, 0])
                    cube([pull_width, pull_width, handle_width]);
            }
            sphere(r=rounding);
        }
    }
    
}

module handle() {
    translate([0, -handle_thick/2, 0])
    {
        intersection() {
            translate([-20, 0, 5]) rotate([0, 65, 0]) rcube([2*handle_height, handle_thick, 3*handle_width], rounding);
            rcube([handle_height, handle_thick, handle_width], rounding);
        }
    }
/*
    translate([0, -support_bar_width/2, 0])
        rcube([handle_height, support_bar_width, handle_thick], rounding);
*/
    translate([handle_height, 0, 0])
            pull();
}

module knob() {
    d1 = 36;
    d2 = 26;
    minkowski() {
        translate([0, 0, rounding])
            cylinder(d1=d1-rounding*2, d2=d2-rounding*2, h=height-rounding*2);
        sphere(r=rounding);
    }
}

difference() {
    union() {
        hull() {
            knob();
            translate([handle_height-1-rounding, 0, rounding])
                minkowski() {
                    //cylinder(d=1, h=height-rounding*2, center=false);
                    #translate([0, -support_bar_width/2+rounding, 0])
                        cube([1, support_bar_width-rounding*2, height-rounding*2]);
                    sphere(r=rounding);
                }
        }
        handle();
    }
    section_3_hole();
    translate([0, 0, square_post_depth]) {
        section_2_hole();
        translate([0, 0, bolt_hole_depth]) {
            section_1_hole();
        }
    }
}



/* vim: set ts=4 sw=4 sts=4 et : */
