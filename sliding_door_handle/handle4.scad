$fn=$preview ? 16 : 64;

rounding = 0.4 * 3;

// section 1
bolt_head_depth = 5.4;
bolt_head_diameter = 13;
section_1_outer_diameter = 17;

module section_1_hole() {
    cylinder(d=bolt_head_diameter, h=bolt_head_depth+20, center=false);
}

// section 2
bolt_hole_depth = 3;
bolt_hole_diameter = 6.5;
module section_2_hole() {
    translate([0, 0, -1])
        cylinder(d=bolt_hole_diameter, h=bolt_hole_depth+2, center=false);
}

// section 3
square_post_width = 18.2;
square_post_depth = 11.5;
section_3_outer_diameter = 29;

module section_3_hole() {
    rotate([0, 0, 45]) 
        translate([-square_post_width/2, -square_post_width/2, 0])
            cube([square_post_width, square_post_width, square_post_depth]);
}

// section 4
rim_inside_diameter_1 = 25;
rim_inside_diameter_2 = 31;
rim_outside_diameter_1 = 29;
rim_outside_diameter_2 = 34.9;

handle_height = 120;
handle_width = 3.0 * 25.4;
handle_thick = 5.5;

height = square_post_depth + bolt_hole_depth + bolt_head_depth;

module rcube(size, rr) {
    minkowski() {
        translate([rr, rr, rr])
            cube([size[0]-2*rr, size[1]-2*rr, size[2]-2*rr]);
        sphere(r=rr);
    }
}
// rounded cylinder by minkowski with sphere
module rcylinder(h, d, center, rr) {
    minkowski() {
        translate([0, 0, center?0:rr]) cylinder(h=h-2*rr, d=d-2*rr, center=center);
        sphere(r=rr);
    }
}

support_bar_width = 20;

pull_width = 1.5 * 25.4;
pull_thick = 0.4*2;

/*
            translate([handle_height-1, 0, rounding])
                minkowski() {
                    cylinder(d=1, h=height-rounding*2, center=false);
                    sphere(r=rounding);
                }
*/

pull_rounding = 0.4*5;

module pull() {
    translate([pull_width/2-pull_thick-rounding, 0, pull_rounding]) {
        minkowski() {
            intersection() {
                difference() {
                    cylinder(h=handle_width-2*rounding, d=pull_width, center=false);
                    cylinder(h=handle_width-2*rounding, d=pull_width-pull_thick*2, center=false);
                }
                translate([-pull_width, -pull_width/2, 0])
                    cube([pull_width, pull_width, handle_width]);
            }
            sphere(r=pull_rounding);
        }
    }
    
}

module handle() {
    translate([0, -handle_thick/2, 0])
    {
        intersection() {
            translate([-20, 0, 5]) rotate([0, 65, 0]) rcube([2*handle_height, handle_thick, 3*handle_width], rounding);
            rcube([handle_height, handle_thick, handle_width], rounding);
        }
    }
/*
    translate([0, -support_bar_width/2, 0])
        rcube([handle_height, support_bar_width, handle_thick], rounding);
*/
    translate([handle_height, 0, 0])
            pull();
}

knob_d1=42;
knob_d2=28;
base_plate_width = 31;

module knob() {
    minkowski() {
        translate([0, 0, rounding])
            cylinder(d1=knob_d1-rounding*2, d2=knob_d2-rounding*2, h=height-rounding*2);
        sphere(r=rounding);
    }
}

wall_thick = 1;
wall_offset = 5;
module knob_side(y) {
    intersection() {
        translate([0, 0, rounding]) {
            difference() {
                cylinder(d1=knob_d1-rounding*2-wall_offset, d2=knob_d2-rounding*2-wall_offset, h=height-rounding*2);
                cylinder(d1=knob_d1-rounding*2-wall_thick-wall_offset, d2=knob_d2-rounding*2-wall_thick-wall_offset, h=height-rounding*2);
            }
        }
        translate([-0.5+wall_offset, (y<0 ? -knob_d1/2 : 0), 0]) cube([1, knob_d1/2, height-rounding*2]);
    }
}

/*
difference() {
    union() {
        hull() {
            knob();
            translate([handle_height-1-rounding, 0, rounding])
                minkowski() {
                    //cylinder(d=1, h=height-rounding*2, center=false);
                    #translate([0, -support_bar_width/2+rounding, 0])
                        cube([1, support_bar_width-rounding*2, height-rounding*2]);
                    sphere(r=rounding);
                }
        }
        handle();
    }
    section_3_hole();
    translate([0, 0, square_post_depth]) {
        section_2_hole();
        translate([0, 0, bolt_hole_depth]) {
            section_1_hole();
        }
    }
}
*/

module new_handle() {

    // handle sides
    for (y=[-1, 1]) {
        minkowski() {
            hull() {
                translate([handle_height+6, y*15.5, rounding])
                    cylinder(d=0.4, h=45-rounding*2);
                intersection() {
                    knob_side(y);
                    translate([-100, -100, 0])
                        cube([200, 200, rounding*2]);
                }
            }
            sphere(r=rounding);
        }
        minkowski() {
            hull() {
                translate([handle_height+6, y*15.5, 45-rounding])
                    cylinder(d=0.4, h=rounding);
                knob_side(y);
            }
            sphere(r=rounding);
        }
    }

    // base between handle sides
    minkowski() {
        translate([knob_d2/2+rounding-1, -base_plate_width/2, rounding])
            cube([handle_height-knob_d2/2-rounding*2+4, base_plate_width, rounding]);
        sphere(r=rounding);
    }
    
    // cross-bar supports between handle sides
    for (x=[0.25, 0.5, 0.75])
        translate([handle_height*x, -base_plate_width/2, 0])
            rcube([0.4*5, base_plate_width, 1+42*x], 0.2);

    // curved handle at top
    translate([handle_height, 0, 0])
            pull();
}

difference() {
    union() {
        knob();
        new_handle();
    }
    section_3_hole();
    translate([0, 0, square_post_depth]) {
        section_2_hole();
        translate([0, 0, bolt_hole_depth]) {
            section_1_hole();
        }
    }
}


/* vim: set ts=4 sw=4 sts=4 et : */
