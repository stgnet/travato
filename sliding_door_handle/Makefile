SOURCES=$(shell find . -name '*.scad')
MODELS=$(SOURCES:%.scad=%.stl)
IMAGES=$(SOURCES:%.scad=%.png)
GCODES=$(SOURCES:%.scad=%.gcode)
CONFIGS=$(shell find . -name '*.ini')

SLICER=slic3r --load anet-et4-pla.ini

all: $(MODELS) $(IMAGES) $(GCODES)
models: $(MODELS)
images: $(IMAGES)
gcodes: $(GCODES)

clean:
	rm *.deps *.stl *.png *.gcode

include $(wildcard *.deps)

%.stl: %.scad
	openscad --render -m make -o $@ -d $@.deps $< 

%.png: %.scad
	openscad --render -o $@ --viewall --autocenter $<
	openscad --render -o rear-$@ --viewall --camera 0,0,0,1,1,1 --autocenter $<

%.gcode: %.stl
	@for f in $(CONFIGS); \
	do \
		PREFIX=$$(basename -s .ini $${f}) ;\
		slic3r --load $${f} --output-filename-format=$$PREFIX-$@ $<;\
	done;

view: $(IMAGES)
	open $(IMAGES)
