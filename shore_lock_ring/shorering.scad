$fn=$preview ? 16 : 64;

inside_diameter = 2.32 * 25.4;
inside_cavity = 2.77 * 25.4;
inner_ring_depth = .175 * 25.4;
outside_diameter = 3 * 25.4;
thick = .6 * 25.4;
outside_bump = 0.35 * 25.4;
outside_rr = .04 * 25.4;

latch_width = .06 * 25.4;
latch_depth = .08 * 25.4;
latch_height = .25 * 25.4;
latch_fudge = .02 * 25.4;

difference() {
    rcylinder(d=outside_diameter, h=thick, center=false, rr=outside_rr);

    translate([0, 0, inner_ring_depth])
        cylinder(d=inside_cavity, h=thick);
    
    cylinder(d2=inside_diameter, d1=inside_diameter+2*inner_ring_depth, h=inner_ring_depth);
    
}

for (r = [0, 180]) {
    rotate([0, 0, r]) {
        translate([inside_cavity/2, -outside_bump/2, 0]) {
            rcube([outside_bump, outside_bump, thick], outside_rr);
        }
        translate([inside_cavity/2 - latch_width, -latch_height/2, thick - latch_depth])
            rcube([latch_width + latch_fudge, latch_height, latch_depth], .01 * 25.4);
    }
}

module rcube(size, rr) {
    minkowski() {
        translate([rr, rr, rr])
            cube([size[0]-2*rr, size[1]-2*rr, size[2]-2*rr]);
        sphere(r=rr);
    }
}
// rounded cylinder by minkowski with sphere
module rcylinder(h, d, center, rr) {
    minkowski() {
        translate([0, 0, center?0:rr]) cylinder(h=h-2*rr, d=d-2*rr, center=center);
            sphere(r=rr);
    }
}
