$fn=$preview ? 16 : 64;

module latch(extra) {

    width = 0.628 * 25.4;
    length = 1.25 * 25.4 + extra;
    thick = 0.14 * 25.4;

    square = 0.31 * 25.4;

    bump = 0.112 * 25.4;

    offset = (length - width)/2;

    difference() {
        hull() {
            for (x = [-1, 1]) {
                translate([offset * x, 0, 0])
                    cylinder(d=width, h=thick);
            }
        }

        translate([-square/2, -square/2, 0])
            cube([square, square, thick]);
    }

    for (r=[0, 180]) {
        rotate([0, 0, r]) {
            translate([-bump/2, width/2 - bump, thick]) {
                cube([bump, bump, bump/2]);
                translate([bump/2, bump, bump/2])
                    rotate([90, 0, 0])
                        cylinder(d=bump, h=bump);
            }
        }
    }
}

for (x = [0:9]) {
    translate([x*18, 0, 0])
        rotate([0, 0, 90])
            latch(x * .05 * 25.4);
}