$fn=$preview ? 8 : 64;

use <nuts_and_bolts_v1.95.scadlib>;

// diameter of hole for insert to fit into
hole_diameter = 0.75 * 25.4;

// distance between flats on either side of the hole
hole_flat = 0.666 * 25.4;

// make nuts slightly larger than bolts
bolt_to_nut_gap = 0.65;

// dimensions of insert
insert_length = 0.66 * 25.4;
insert_width = 3/4 * 25.4;
insert_width_nut = insert_width + bolt_to_nut_gap;
insert_flange_thick = 1.5;
insert_flange_diameter = 1 * 25.4;
insert_core_diameter = 13;

// knob size
knob_diameter = 1 * 25.4;
knob_thick = 0.55 * 25.4;
// knob finger detents
knob_cutout_radius = 0.6 * 25.4;
knob_cutout_height = 0.3 * 25.4;
knob_cutout_offset = knob_diameter/2 + 0.15 * 25.4;
// knob smoothing
knob_rounding = 3;
knob_core_diameter = knob_diameter - 2*knob_rounding;
knob_core_thick = knob_thick - knob_rounding;

// shaft size (on top of knob)
knob_shaft_gap = 0.5;
knob_shaft_diameter = insert_core_diameter - knob_shaft_gap;
knob_shaft_length_gap = .3;
knob_shaft_length = insert_length + insert_flange_thick + knob_shaft_length_gap;

// square interface to lock plate (on top of shaft)
// pow(knob_shaft_diameter, 2) = 2 * pow(square_diameter, 2)
// pow(knob_shaft_diameter, 2) / 2 = pow(square_diameter, 2)
// sqrt(pow(knob_shaft_diameter, 2)/2) = square_diameter
square_diameter = sqrt(pow(knob_shaft_diameter, 2)/2);
square_thick = 4;

// bolt to attach lock plate (on top of square interface)
lock_bolt_length = 4;
lock_bolt_width = square_diameter;
lock_bolt_nut = lock_bolt_width + bolt_to_nut_gap;

// lock plate to engage catch
lock_plate_thick = square_thick + 0.5;
lock_plate_radius = 1.15 * 25.4;
lock_square_diameter = square_diameter + bolt_to_nut_gap;

// notches out of lock plate with angles towards catch
notch_radius = 140;
notch_offset = 120;
notch_raise = 90;

// detents on lock plate for catch
detent_height = 1/2 * 25.4;
detent_width = 1/4 * 25.4;
detent_depth = 1;
detent_radius = sqrt(pow(detent_height/2, 2) + pow(detent_width, 2));

// put on the objects on the build plate large side down

knob();

translate([0, 0, knob_thick]) {
    shaft();
}
translate([30, 0, 0]) {
    shaft_nut();
}

translate([0, 30, 0]) {
    insert();
    translate ([30, 0, 0]) {
        insert_nut();
    }
}

translate([lock_plate_radius-15, 40+lock_plate_radius, 0]) {
    lock_plate();
}

// modules for each object defining how to build it

module insert() {
    difference () {
        union() {
            intersection() {
                translate([0, 0, insert_flange_thick])
                    hex_bolt(insert_length, insert_width, 0, 0, 1/128, 32, 1, "metric", 2.54);

                // flatten the sides of the bolt to fit the hole
                translate([-hole_diameter/2+(hole_diameter-hole_flat)/2, -hole_diameter/2, -10])
                    cube([hole_flat, hole_diameter+1, 40]);
            }
            cylinder(d=insert_flange_diameter, h=insert_flange_thick, center=false);
        }
        // remove the core for the knob
        translate([0, 0, -insert_core_diameter])
            cylinder(d=insert_core_diameter, h=40, center=false);
    }
}

module insert_nut() {
    hex_nut (0.2 * 25.4, insert_width_nut, insert_flange_diameter, 1/128, 32, 1, "metric", 2.54);
}

module knob() {
    intersection() {
        minkowski() {
            translate([0, 0, knob_rounding]) {
                difference() {
                    cylinder(d=knob_core_diameter, h=knob_core_thick, center=false);
                    translate([0, -knob_cutout_offset, knob_cutout_height - knob_rounding]) {
                        sphere(r=knob_cutout_radius);
                    }
                    translate([0, knob_cutout_offset, knob_cutout_height - knob_rounding]) {
                        sphere(r=knob_cutout_radius);
                    }
                }
            }
            sphere(r=knob_rounding);
        }
        cylinder(d=knob_diameter, h=knob_thick, center=false);
    }
}

module shaft() {
    cylinder(d=knob_shaft_diameter, h=knob_shaft_length, center=false);

    translate([-square_diameter/2, -square_diameter/2, knob_shaft_length]) {
        cube([square_diameter, square_diameter, square_thick]);
    }
    translate([0, 0, knob_shaft_length + square_thick]) {
        hex_bolt(lock_bolt_length, lock_bolt_width, 0, 0, 1/128, 32, 1, "metric", 1);
    }
}

module shaft_nut() {
    //  hex_nut (height, thread_d, size, tolerance, quality, thread, pitch);
        hex_nut (3, lock_bolt_nut, lock_bolt_width+5, 1/128, 32, 1, "metric", 1);
}

module lock_plate() {
    difference() {
        cylinder(r=lock_plate_radius, h=lock_plate_thick);

        translate([-lock_square_diameter/2, -lock_square_diameter/2, -1]) {
            cube([lock_square_diameter, lock_square_diameter, lock_plate_thick+2]);
        }
        for (side = [-1, +1]) {
            translate([0, side * notch_offset, notch_raise]) sphere(r=notch_radius);
        
            translate([side * lock_plate_radius, 0, lock_plate_thick - detent_depth])
                cylinder(r1=detent_radius/2, r2=detent_radius, h=detent_depth);
        }
    }
}

// UNUSED DOOR FRAME SIMULATION

module hole() {
    diameter = 0.75 * 25.4;
    flat = 0.666 * 25.4;

    intersection() {
        cylinder(d=diameter, h=5);
        translate([-diameter/2+(diameter-flat)/2, -diameter/2, 0])
            cube([flat, diameter+1, 5]);
    }
}

module door() {
    thickness = 0.1 * 25.4;
    wall2flat = 0.666 * 25.4;
    wall2center = 0.666 * 25.4 /2  + wall2flat;

    translate([0, 0, -thickness]) {
        difference() {
            cube([80, 80, thickness]);
            translate([wall2center, 40, -1]) hole();
        }
    }

    translate([-thickness, 0, -20])
        cube([thickness, 80, 20]);

    translate([0, 80/2 - 5, -0.65*25.4 -thickness])
        cube([4.3, 10, thickness]);
}

//%door();
