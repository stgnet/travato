$fn=$preview ? 16 : 60;

cylinder_diameter = 3.25 * 25.4;
cylinder_height = 10 * 25.4;

cylinder_top_height = 1.25 * 25.4;

cylinder_neck_diameter = 1.3 * 25.4;
cylinder_neck_height = 0.75 * 25.4;

module kiddie() {
    cylinder(d=cylinder_diameter, h=cylinder_height);
    translate([0, 0, cylinder_height]) {
            cylinder(d1=cylinder_diameter, d2=cylinder_neck_diameter, h=cylinder_top_height);
        
        translate([0, 0, cylinder_top_height])
            cylinder(d=cylinder_neck_diameter, h=cylinder_neck_height);
    }
}


rounding = 1.5;
gap = 3;
holder_base_height = 3;
holder_rim_width = 6;
holder_base_rim_height = 20;
holder_ring_rim_height = 25;
holder_ring_offset = 90;
holder_screw1_offset = 40;
holder_screw2_offset = 80;
attachment_width = 42;
attachment_thick = 6;
base_support_depth = 16;

module ring(outside, inside, height) {
    difference() {
        cylinder(d=outside, h=height);
        cylinder(d=inside, h=height);
    }
}

module rcube(size, rr) {
    minkowski() {
        translate([rr, rr, rr])
            cube([size[0]-2*rr, size[1]-2*rr, size[2]-2*rr]);
        sphere(r=rr);
    }
}
// rounded cylinder by minkowski with sphere
module rcylinder(h, d, center, rr) {
    minkowski() {
        translate([0, 0, center?0:rr]) cylinder(h=h-2*rr, d=d-2*rr, center=center);
        sphere(r=rr);
    }
}

outside_diameter = cylinder_diameter + 2 * holder_rim_width + gap;
inside_diameter = cylinder_diameter + gap;

module screw() {
    steps = [
        [8.5, 0],
        [8.5, 1],
        [4, 4],
        [4, 18],
        [0, 26]
    ];
    screw_gap = 0.3;
    for (i = [0: len(steps)-2]) {
        translate([0, 0, -steps[i+1][1]])
            cylinder(
                d1=steps[i+1][0] + screw_gap*2,
                d2=steps[i][0] + screw_gap*2,
                h=steps[i+1][1]-steps[i][1]);
    }
}

translate([0, 0, outside_diameter/2]) rotate([0, 90, 0]) {
    difference() {
        translate([inside_diameter/2, -attachment_width/2, -base_support_depth]) {
            rcube([
                    attachment_thick, 
                    attachment_width,
                    holder_ring_offset+holder_ring_rim_height+base_support_depth
                ],
                rounding
            );
        }
        translate([inside_diameter/2, 0, holder_screw1_offset])
            rotate([0, -90, 0]) 
                #screw();
        translate([inside_diameter/2, 0, holder_screw2_offset])
            rotate([0, -90, 0]) 
                #screw();
    }

    hull() {
        minkowski() {
           translate([0, 0, rounding])
                cylinder(d=outside_diameter-rounding*2, h=rounding);
            sphere(r=rounding);
        }
        translate([inside_diameter/2, -attachment_width/2, -base_support_depth])
            rcube([attachment_thick, attachment_width, attachment_thick], rounding);
    }
     
    minkowski() {
        union() {
            translate([0, 0, rounding])
                cylinder(d=outside_diameter-rounding*2, h=holder_base_height-rounding);
            translate([0, 0, holder_base_height]) {
                ring(outside_diameter-rounding*2, inside_diameter+rounding*2,
                    holder_base_rim_height-rounding);
            }
        }
        sphere(r=rounding);
    }

    translate([0, 0, holder_ring_offset]) {
        minkowski() {
            translate([0, 0, rounding])
                ring(outside_diameter-rounding*2, inside_diameter+rounding*2, holder_ring_rim_height-rounding*2);

            sphere(r=rounding);
        }
    }


    translate([0, 0, holder_base_height])
        %kiddie();
}


