SOURCES=$(shell find . -name '*.scad' -maxdepth 1)
MODELS=$(SOURCES:%.scad=%.stl)
IMAGES=$(SOURCES:%.scad=%.png)
GCODES=$(SOURCES:%.scad=%.gcode)
CONFIGS=$(shell find . -name '*.ini' -maxdepth 1)

all: $(MODELS) $(IMAGES) $(GCODES)
models: $(MODELS)
images: $(IMAGES)
gcodes: $(GCODES)

clean:
	rm *.deps *.stl *.png *.gcode

include $(wildcard *.deps)

%.stl: %.scad
	openscad --render -m make -o $@ -d $@.deps $<

%.png: %.scad
	openscad --render -o $@ --viewall --autocenter $<
	openscad --render -o rear-$@ --viewall --camera 0,0,0,1,1,1 --autocenter $<

%.gcode: %.stl
	@for f in $(CONFIGS); \
	do \
		PREFIX=$$(basename -s .ini $${f}) ;\
		which slic3r && slic3r --load $${f} --output-filename-format=$$PREFIX-$@ $< || echo Skipping slicing of $@;\
	done;

view: $(IMAGES)
	open $(IMAGES)
