// Winnebago Travato 59G - Shower Drain Cover
// Scott Griepentrog, 2019-10-01, scott@stg.net
// Part is [no longer] printed upside down
// Funnels water in drain depression into drain connector (water hose thread)
// Prevents dirty water accumulating around fitting
// https://gitlab.com/stgnet/travato

$fn=$preview ? 16 : 64;

include <tsmthread4.scadlib>;

// label embedded into base
text="59G SDC v3.3 stg.net";
text_depth = 0.05;
text_offset = 0.81;
text_size = 0.31;

// height of the entire unit, which screws down over threaded fitting in depression
height=0.75;

// increase the height of the actual thread calculation to insure it's removed cleanly
fudge = 0.08;

seal_height = height-0.40;
channel_height = 0.06;

// make 4 in 2x2 grid for faster? printing
fourup=false;
diameter = 4.2;

// build simpler version for whale gulper pump
whale=false;
whale_flat = 0.75;
whale_height = 0.1;
whale_hole = 0.1;

dimensions=[
    [diameter, 0],
    [diameter, 0.06],
    [3, 0.45],
    [2.5, height],
];

// drainage slots slanted toward center
channel_width= 5/64;

// offset of slot above seal ring (how much plastic is left below slot)
slot_offset = 5/64;

// diamater of the drain holes
drain_hole = 5/128;

// thickness of solid material above "underground" drain channel slots
drain_height = 4/128;

// distance between drain holes
drain_d = 0.25;
// angle pattern between drain holes
hexa = (360/6);

// washer to provide seal against top of connector (3/4" ID)
washer_height = height-0.38;
washer_thick = washer_height - seal_height + fudge;
washer_hole = 1/2;

module washer() {
    translate([0, 0, washer_height-washer_thick]) {
        difference() {
            cylinder(d=1+1/2, h=washer_thick, center=false);
            translate([0, 0, -fudge])
                cylinder(d=washer_hole, h=washer_thick+fudge*2, center=false);
        }
    }
}

funnel_od = 9/16;
funnel_id = 7/16;
funnel_height =  0.25;
funnel_cone = 0.14;
funnel_support_height = 0.8/25.4;

module funnel(remove) {
    spout=height-funnel_height-funnel_cone;
    head2=0.4*2/25.4;
    
    // upper cone
    translate([0, 0, funnel_height]) {
        difference() {
            cylinder(d1=funnel_od+funnel_cone*3, 
                     d2=funnel_od,
                     h=funnel_cone);
            if (!remove)
            cylinder(d1=funnel_id+funnel_cone*3,
                     d2=funnel_id,
                     h=funnel_cone);
        }
        if (!remove) intersection() {
            union() {
                cylinder(d1=funnel_od+funnel_cone*3,
                         d2=funnel_od, h=funnel_cone);
                cylinder(d=funnel_od, h=funnel_cone+spout);
            }
            union()
                for (r=[0:hexa:359])
                    rotate([0, 0, r+hexa/2])
                        translate([0, -head2/2, 0])
                            cube([funnel_od+funnel_cone*3, head2, height]);        
        }
    }

    translate([0, 0, funnel_height+funnel_cone]) {
        difference() {
            cylinder(d=funnel_od, h=spout);
            if (!remove)
            cylinder(d=funnel_id, h=spout);
        }
    }
       
}


module diamond(d) {
    for (s=[0, 180])
        rotate([s, 0, 0])
            cylinder(d2=0, d1=d, h=d/2, $fn=4);
}

module drain(depth) {
    hull() {
        cylinder(
            d1=drain_hole,
            d2=channel_width,
            h=drain_height, center=false);
        translate([0, 0, channel_height])
            sphere(d=channel_width);
    }

    cw_add = 0; // (8-depth)/100;
    ch_add = (8-depth)/40;

    // drain channels
    translate([0, 0, channel_height])
        hull() {
            translate([0, 0, ch_add]) 
                diamond(d=channel_width+cw_add);
            diamond(d=channel_width+cw_add);
            translate([-drain_d, 0, ch_add])
                diamond(d=channel_width+cw_add);
            translate([-drain_d, 0, 0])
                diamond(d=channel_width+cw_add);
        }
        
    
    if (depth==1) {
        translate([drain_d, 0, 0])
            drain(depth+1);
    } else if (depth<8) {
        for (side=[-1, 1])
            rotate([0, 0, side*hexa]) 
                translate([drain_d, 0, 0])
                    drain(depth+1);
    }
}

module cover() {
    translate([0, 0, height]) rotate([180, 0, 0]) {
        difference() {
            union() {
                for (i=[0:len(dimensions)-2]) {
                    translate([0, 0, dimensions[i][1]])
                        cylinder(
                            d1=dimensions[i][0],
                            d2=dimensions[i+1][0],
                            h=dimensions[i+1][1]-dimensions[i][1],
                            center=false);
                }
            }
            
            // take out a quarter for testing
            // cube([100, 100, 100]);

            // standard "water hose" thread
            translate([0, 0, -fudge + seal_height])
                if ($preview) {
                    cylinder(d=1+1/16, h=height+2*fudge - seal_height);
                } else {
                    tsmthread((1+1/16)+$ID_COMP, 
                        height+2*fudge - seal_height, 
                        PITCH=1/11.5,
                        TAPER=0,
                        PR=THREAD_NH);
                }
          
            if (!$preview && !whale)
                for (r=[0:hexa:359]) {
                    rotate([0, 0, r])
                        translate([drain_d, 0, 0])
                            drain(1);
            }

    /* this slows down print
            // label the version into the base
            tr=360/(len(text)+1);
            for (i=[0:len(text)-1])
                rotate([0, 0, -tr*i])
                    translate([0, text_offset, height-text_depth])
                        linear_extrude(text_depth)
                            text(text[i], size=text_size, halign="center");
    */
            
            if (whale) {
                cylinder(h=whale_height, d1=diameter-0.25, d2=whale_flat);
                for (r=[0:hexa:359])
                    rotate([0, 0, r])
                        translate([whale_flat/3, 0, 0])
                            cylinder(h=funnel_height+funnel_cone, d1=whale_hole, d2=whale_hole*2);
                        
//                    cylinder(h=funnel_height+funnel_cone-whale_height, d=whale_hole);
                    // or without funnel:
                    // cylinder(h=0.171, d1=whale_hole, d2=1.1023);
            } else {
                funnel(1);
            }
        }

        // add back the outer walls of the funnel
//        if (!whale)
            funnel();
    }
}

if (fourup) {
    gap = .02;
    imperial() translate([diameter/2, diameter/2, 0]) {
        for (y=[0,diameter+gap]) for (x=[0,diameter+gap]) translate([x, y, 0])
            cover();
    }
} else {
    imperial()
        translate([diameter/2, diameter/2, 0])
            cover();
}