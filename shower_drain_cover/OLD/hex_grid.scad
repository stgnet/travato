$fn=16;

//#translate([0, 0, -10]) cylinder(d=4 * 25.4, h=1);

a=(360/6);
d=.2;

// drainage slots slanted toward center
channel_width= 6/128;

// offset of slot above seal ring (how much plastic is left below slot)
slot_offset = 5/64;

// diamater of the drain holes
drain_hole = 5/128;

// thickness of solid material above "underground" drain channel slots
drain_height = 4/128;

channel_height=0.1;


module diamond(d) {
    for (s=[0, 180])
        rotate([s, 0, 0])
            cylinder(d2=0, d1=d, h=d/2, $fn=4);
}

module drain(depth) {
    hull() {
        #cylinder(
            d2=drain_hole,
            d1=channel_width,
            h=drain_height, center=false);
        translate([0, 0, -channel_height])
            sphere(d=channel_width);
    }

    cw_add = 0; // (8-depth)/100;
    ch_add = (8-depth)/50;

    // drain channels
    translate([0, 0, -channel_height])
        hull() {
            translate([0, 0, -ch_add]) 
                diamond(d=channel_width+cw_add);
            diamond(d=channel_width+cw_add);
            translate([-d, 0, -ch_add])
                diamond(d=channel_width+cw_add);
            translate([-d, 0, 0])
                diamond(d=channel_width+cw_add);
        }
        
    
    if (depth==1) {
        translate([d, 0, 0])
            drain(depth+1);
    } else if (depth<8) {
        for (side=[-1, 1])
            rotate([0, 0, side*a]) 
                translate([d, 0, 0])
                    drain(depth+1);
    }
}

scale([25.4, 25.4, 25.4])
for (r=[0:a:359]) {
    rotate([0, 0, r])
        translate([d, 0, 0])
            drain(1);
}