// Winnebago Travato 59G - Shower Drain Cover
// Scott Griepentrog, 2019-10-01, scott@stg.net
// Part is printed upside down
// Funnels water in drain depression into drain connector (water hose thread)
// Prevents dirty water accumulating around fitting
// https://gitlab.com/stgnet/travato

$fn=$preview ? 16 : 64;

include <tsmthread4.scadlib>;

// label embedded into base
text="59G SDC-S v1.6 stg.net";
text_depth = 0.03;
text_offset = 0.68;

// height of the entire unit, which screws down over threaded fitting in depression
height=0.6;

// increase the height of the actual thread calculation to insure it's removed cleanly
fudge = 0.1;

// seal is a cone from diameter width to 0 over height - presses against edge around drain hole
seal_width = 4.2;
seal_start = 0.03;
seal_height = 1/4;

// drainage slots slanted toward center
slot_width= 5/64;

// offset of slot above seal ring (how much plastic is left below slot)
slot_offset = 3/64;

// diamater of the drain holes
drain_hole = 2/64;

// thickness of solid material above "underground" drain channel slots
drain_height = 3/128;

// everything is in inches (but output metric)
imperial() {
    difference() {
        union() {
            // wide disc that seals against side walls of depression
            cylinder(d=seal_width, h=seal_start, center=false);
            translate([0, 0, seal_start])
                cylinder(d1=seal_width, d2=0, h=seal_height-seal_start, center=false);     
            // center cylinder around threads
            cylinder(d1=3, d2=2, h=height, center=false);
        }

        // standard "water hose" thread
        translate([0, 0, -fudge + seal_height])
            if ($preview) {
                cylinder(d=1+1/16, h=height+2*fudge - seal_height);
            } else {
                tsmthread((1+1/16)+$ID_COMP, 
                    height+2*fudge - seal_height, 
                    PITCH=1/11.5,
                    TAPER=0,
                    PR=THREAD_NH);
            }
        
        // drain slots around the threads
        intersection() {
            // cut slots around the threads for drainage
            for (r=[0:20:($preview?40:359)])
                rotate([0, 0, r]) {
                    // the slot
                    translate([.4, -slot_width/2, drain_height])
                        cube([1.5, slot_width, seal_height]);
                    // cut small drain holes
                    for (d=[0.4:0.1:1.5])
                        translate([d, 0, 0]) {
                            cylinder(d1=drain_hole, d2=slot_width, h=drain_height, center=false);
                            translate([0, 0, drain_height])
                                cylinder(d=slot_width, h=seal_height-drain_height, center=false);
                            /*
                            if (d>0.5) {
                                for (y=[0.07, -0.07]) {
                                    // adjacent drain holes
                                    translate([0.05, y, 0])
                                        cylinder(d=drain_hole, h=seal_height, center=false);
                                    // .. with channels back to drain slot
                                    translate([0, 0, drain_height]) hull() {
                                        cylinder(d=slot_width, h=seal_height, center=false);
                                        translate([0.05, y, 0])
                                            cylinder(d=slot_width, h=seal_height, center=false);
                                    }
                                }
                            }
                            */
                        }
                }
            // repeat the seal cone raised by offset to trim slot depth
            translate([0, 0, -slot_offset])
                cylinder(d1=seal_width, d2=0, h=seal_height, center=false);
        }


        // label the version into the base
        for (i=[0:len(text)-1])
            rotate([0, 0, -15*i])
                translate([0, text_offset, height-text_depth])
                    linear_extrude(text_depth)
                            text(text[i], size=0.25, halign="center");
    }

}
