// Winnebago Travato 59G - Shower Drain Cover
// Scott Griepentrog, 2019-10-01, scott@stg.net
// Part is printed upside down
// Funnels water in drain depression into drain connector (water hose thread)
// Prevents dirty water accumulating around fitting
// https://gitlab.com/stgnet/travato

$fn=$preview ? 16 : 64;

include <tsmthread4.scadlib>;

// label embedded into base
text="59G SDC-S2 v2.0 stg.net";
text_depth = 0.03;
text_offset = 0.68;

// height of the entire unit, which screws down over threaded fitting in depression
height=0.6;

// increase the height of the actual thread calculation to insure it's removed cleanly
fudge = 0.08;

// seal is a cone from diameter width to 0 over height - presses against edge around drain hole
seal_width = 4.2;
seal_start = 0.03;
seal_height = height-0.40;


dimensions=[
    [seal_width, 0],
    [seal_width, 0.06],
    [3, seal_height],
    [2, height],
];

// drainage slots slanted toward center
slot_width= 5/64;

// offset of slot above seal ring (how much plastic is left below slot)
slot_offset = 5/64;

// diamater of the drain holes
drain_hole = 5/128;

// thickness of solid material above "underground" drain channel slots
drain_height = 4/128;

// washer to provide seal against top of connector (3/4" ID)
washer_height = height-0.38;
washer_thick = washer_height - seal_height + fudge;
washer_hole = 1/2;

module washer() {
    translate([0, 0, washer_height-washer_thick]) {
        difference() {
            cylinder(d=1+1/2, h=washer_thick, center=false);
            translate([0, 0, -fudge])
                cylinder(d=washer_hole, h=washer_thick+fudge*2, center=false);
        }
    }
}

// everything is in inches (but output metric)
imperial() rotate([180, 0, 0]) {
    difference() {
        union() {
            for (i=[0:len(dimensions)-2]) {
                translate([0, 0, dimensions[i][1]])
                    cylinder(
                        d1=dimensions[i][0],
                        d2=dimensions[i+1][0],
                        h=dimensions[i+1][1]-dimensions[i][1],
                        center=false);
            }
            washer();
        }

        // standard "water hose" thread
        translate([0, 0, -fudge + seal_height])
            if ($preview) {
                cylinder(d=1+1/16, h=height+2*fudge - seal_height);
            } else {
                tsmthread((1+1/16)+$ID_COMP, 
                    height+2*fudge - seal_height, 
                    PITCH=1/11.5,
                    TAPER=0,
                    PR=THREAD_NH);
            }
        
        max_diameter = 1.75;
        // drain slots around the threads
        intersection() {
            // cut slots around the threads for drainage
            for (r=[0:40:($preview?40:359)])
                rotate([0, 0, r]) {
                    // the slot
                    translate([.2, -slot_width/2, drain_height])
                        cube([1.5, slot_width, seal_height]);
                    // cut small drain holes
                    for (d=[0.2:0.1:max_diameter])
                        translate([d, 0, 0]) {
                            cylinder(d1=drain_hole, d2=slot_width, h=drain_height, center=false);
                            translate([0, 0, drain_height])
                                cylinder(d=slot_width, h=seal_height-drain_height, center=false);
                            if (d>0.3) {
                                for (p=[-1, 1]) for (i=[1:d*8.5]) {
                                    x = 0.03 * i;
                                    y = p * 0.05 * i;
                                    if (d + x + p*y/5 < max_diameter) {
                                        // adjacent drain holes
                                        translate([x, y, 0])
                                            cylinder(d1=drain_hole, d2=slot_width/2, h=drain_height, center=false);
                                        // .. with channels back to drain slot
                                        translate([0, 0, drain_height]) hull() {
                                            cylinder(d=slot_width/2, h=seal_height, center=false);
                                            translate([x, y, 0])
                                                cylinder(d=slot_width/2, h=seal_height, center=false);
                                        }
                                    }
                                }
                            }                            
                        }
                }
            // repeat the seal cone raised by offset to trim slot depth
            i=1;
            translate([0, 0, -slot_offset])
                translate([0, 0, dimensions[i][1]])
                    cylinder(
                        d1=dimensions[i][0],
                        d2=dimensions[i+1][0],
                        h=dimensions[i+1][1]-dimensions[i][1],
                        center=false);
        }


        // label the version into the base
        for (i=[0:len(text)-1])
            rotate([0, 0, -15*i])
                translate([0, text_offset, height-0.01])
                    minkowski() {
                        linear_extrude(text_depth)
                            text(text[i], size=0.25, halign="center");
                            if (!$preview)
                                cylinder(d1=0.00, d2=text_depth, h=text_depth-0.01);
                    }
    }

    // washer is repeated outside the thread removal
    washer();
}
