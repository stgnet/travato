// curtain track rail
$fn=$preview ? 16 : 64;

/*
ideas for interlocking pieces together:
- two types of inserts
  - between rails, fits in gap between base
  - end cap serves as clip block (how to firmly attach?)
* issue is to resolve printability of overhang of interlock  
*/

rounding = 0.2;

top_rail_y = 14.6; // with squish 15.0 ends up being 15.6
top_rail_z = 3.8;

total_z = 8.5;
base_y = 10.6;
base_z = total_z - top_rail_z;
base_wall_y = 3.3;

// original track is 9" between holes
track_length = 8 * 25.4;
screw_head_depth_z = 3;
screw_head_d = 8.8;
hole_d = 5;

base_gap_y = base_y-base_wall_y*2;

wedge_max_y = base_gap_y*1.333;

print_x = 7.5 * 25.4;
print_y = 7.5 * 25.4;
//#cube([print_x, print_y, 0.1]);

module rcube(size, r_arg) {
    r = r_arg ? r_arg: rounding;
    minkowski() {
        translate([r, r, r])
            cube([size[0]-2*r, size[1]-2*r, size[2]-2*r]);
        sphere(r=r);
    }
}

module repeat(v, count) {
    i = v/(count+1);
    for (r = [1:count]) {
        translate(r*i)
            children();
    }
}
module repeat_even(v, count) {
    i = v/count;
    for (r = [0.5:1:count]) {
        translate(r*i)
            children();
    }
}

module hole() {
    translate([0, top_rail_y/2, 0]) {
        cylinder(d1=screw_head_d, d2=hole_d, h=top_rail_z);
        translate([0, 0, top_rail_z])
            cylinder(d=hole_d, h=base_z);
    }
}

module screw() {
    // reinforcement in base for screw hole
    translate([0, top_rail_y/2, top_rail_z])
        cylinder(d=base_y-rounding*2, h=base_z);
}

module wedge(length, remove) {
    extra = remove? 0.15 : 0;
    hull() for (side=[0:1]) translate([length*side, 0, 0])
        cylinder(d1=wedge_max_y+extra*4, d2=base_gap_y+extra*2, h=base_z);
}


module rail(l) {
    fudge = rounding * 2; // fudge base into rail to eliminate rounded edges
    length = l ? l : track_length;

    difference() {
        union() {
            rcube([length, top_rail_y, top_rail_z]);
            translate([0, (top_rail_y-base_y)/2, top_rail_z-fudge]) {
                difference() {
                    union() {
                        rcube([length, base_wall_y, base_z+fudge]);
                        translate([0, base_wall_y+base_gap_y, 0])
                            rcube([length, base_wall_y, base_z+fudge]);
                    }
                    translate([0, base_wall_y + base_gap_y/2, fudge])
                        wedge(length, 1);
                }
            }
            repeat_even([length, 0, 0], 2) screw();
        }
        repeat_even([length, 0, 0], 2) #hole();
    }
}

count=8;
wedge_length = 0.75*25.4;

for (i=[0:count-1]) {
    translate([0, i*(top_rail_y+1), 0])
        rail();

    translate([wedge_max_y/2 + (wedge_max_y + wedge_length + 1) * i, -4, 0]) 
        wedge(wedge_length,0);
}
