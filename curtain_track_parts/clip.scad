// curtain track clip
$fn=$preview ? 16 : 64;

clip_y = 19.4;
clip_x = 12.4;
clip_z = 11.2;

rounding = 0.5;

track_y = 16.0; // track is 15.0
track_z = 4.6; // track is 3.8
clip_catch_overhang_y = 4.0;

snap_hole_d = 7.0;
snap_hole_plate_z = 1.6;

support_wall_x = 1.6;
support_wall_y = clip_catch_overhang_y;
track_offset_z = 3.3;

clip_wall_y = (clip_y - track_y)/2;
track_top_z = track_offset_z + track_z;


module rcube(size, rr) {
    minkowski() {
        translate([rr, rr, rr])
            cube([size[0]-2*rr, size[1]-2*rr, size[2]-2*rr]);
        sphere(r=rr);
    }
}

module clip() {
    rotate([0, 90, 0]) {

        /*
        difference() {
            rcube([clip_x, clip_y, snap_hole_plate_z], rounding);
            translate([clip_x/2, clip_y/2, 0])
            cylinder(d=snap_hole_d, h=snap_hole_plate_z);
        }
        */

        // base with rounded hole
        minkowski() {
            translate([rounding, rounding, rounding]) {
            difference() {
                x=clip_x-2*rounding;
                y=clip_y-2*rounding;
                cube([x, y, snap_hole_plate_z-2*rounding]);
                translate([x/2, y/2, 0])
                cylinder(d=snap_hole_d+2*rounding, h=snap_hole_plate_z);
            }
            }
            sphere(r=rounding);
        }

        for (side=[0:1]) translate([0, clip_y*side, 0]) mirror([0, side, 0]) {
            rcube([clip_x, clip_wall_y, clip_z], rounding);

            translate([0, 0, track_top_z])
            hull() {
                rcube([clip_x, clip_catch_overhang_y, rounding*2.1], rounding);
                rcube([clip_x, clip_wall_y, clip_z-track_top_z], rounding);
            }
        }

        for (side=[0:1]) translate([side * (clip_x-support_wall_x),0, 0]) {
           rcube([support_wall_x, clip_y, track_offset_z], rounding);
        }
    }
}

for (x=[0:5])
    for (y=[0:5])
        translate([x*(clip_x+2), y*(clip_y+2), 0])
            clip();
    