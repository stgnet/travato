$fn=$preview ? 16 : 60;

box_depth = 6.5 * 25.4;
box_width = 1.5 * 25.4;
box_thick = 3/64 * 25.4;
pin_diameter = 3/4 * 25.4;
pin_depth = 2 * 25.4;
pin_gap = 1;
pin_spring_thick = box_thick;
pin_spring_depth = 1.75 * 25.4;
cap_width = 2 * 25.4;
cap_thick = 1/8 * 25.4;
rounding = 4/16 * 25.4;
corner = 1/8 * 25.4;

cap_x = cap_width - 2*rounding;
pin_thick = (cap_width - box_width)/2;
box_side = box_width - corner *2;
corner_side = sqrt(pow(corner, 2) * 2);


module box() {

    hull() {
        for (x = [-1, 1]) {
            for (y = [-1, 1]) {
                translate([cap_x/2*x, cap_x/2*y, 0])
                    cylinder(d=rounding, h=cap_thick);
            }
        }
    }
    translate([0, 0, cap_thick]) {

        // box sides
        for (r = [0, 90, 180, 270]) {
            rotate([0, 0, r]) {
                difference() {
                    translate([-box_side/2, box_width/2-box_thick, 0])
                        cube([box_side, box_thick, box_depth]);

                    if (r%180 == 0) {
                        // create hole for pin and spring
                        translate([0, box_width/2, pin_depth])
                            rotate([90, 0, 0]) {
                                hull() {
                                    cylinder(d=pin_diameter, h=box_thick);
                                    translate([-pin_diameter/2, -pin_spring_depth, 0])
                                        cube([pin_diameter, box_thick, box_thick]);
                            }
                        }
                    }
                }
                if (r%180 == 0) {
                    translate([0, box_width/2, pin_depth]) {
                        rotate([90, 0, 0]) {
                            spring_diameter = pin_diameter - pin_gap*2;
                            translate([0, 0, -pin_thick])
                                cylinder(d1=spring_diameter - pin_thick*2, d2=spring_diameter, h=pin_thick);
                            hull() {
                                cylinder(d=spring_diameter, h=pin_spring_thick);
                                translate([-spring_diameter/2, -pin_spring_depth, 0])
                                    cube([spring_diameter, pin_spring_thick, pin_spring_thick]);
                            }
                        }
                    }
                }

                // corner at 45 degrees
                translate([box_side/2, box_width/2, 0])
                    rotate([0, 0, 180-45])
                        translate([-corner_side, 0, 0])
                            cube([corner_side, box_thick, box_depth]);
            }
        }
        
        // end cap
        /*
        translate([0, 0, box_depth - cap_thick])
            for (r = [0, 90]) {
                rotate([0, 0, r]) {
                    translate([-box_side/2, -box_width/2, 0])
                        cube([box_side, box_width, cap_thick]);
                }
            }
        */
    }
}

box();
