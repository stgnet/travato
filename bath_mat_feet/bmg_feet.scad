$fn=$preview ? 16 : 64;

board_z = 0.75;
board_y = 3.5;

deck_z = 1.15;
door_offset_y = 0.55;

rounding = 0.05;

lower_z = deck_z - board_z;
lower_x = 1;
board_gap = 0.25;
tab_height = 0.25;
door_tab_height = 0.2;
door_tab_width = rounding*2.5;

lower_gap_z = 0.20;

connector_length = 4;
connector_width = 1;
connector_thick = 1/8;

connector_hole_d = 5/8;
connector_fudge = 0.01;


round = true;

module rcube(size, rr) {
    minkowski() {
        translate([rr, rr, rr])
            cube([size[0]-2*rr, size[1]-2*rr, size[2]-2*rr]);
        sphere(r=rr);
    }
}

module rcylinder(h, d, center, rr) {
    minkowski() {
        translate([0, 0, center?0:rr]) cylinder(h=h-2*rr, d=d-2*rr, center=center);
            sphere(r=rr);
    }
}

module rcylinder_hole(h, d, center, rr) {
    difference() {
        cylinder(h=h, d=d+2*rr);
        rotate_extrude(convexity=10) {
            translate([d/2+rr, rr, 0]) {
                hull() {
                    circle(r=rr);
                    translate([0, h-2*rr, 0]) circle(r=rr);
                }
            }
        }
    }
}

module screw() {
    steps = [
        [8.5, 0],
        [8.5, 1],
        [4, 4],
        [4, 18],
        [0, 26]
    ];
    screw_gap = 0.3;
    for (i = [0: len(steps)-2]) {
        translate([0, 0, -steps[i+1][1]])
            cylinder(
                d1=steps[i+1][0] + screw_gap*2,
                d2=steps[i][0] + screw_gap*2,
                h=steps[i+1][1]-steps[i][1]);
    }
}

module boards() {
    for (y=[0:3.75:18]) translate([0, y, 0]) {
        color("tan")
        if (y>14) {
            cube([40, 1.25, board_z]);
        } else {
            cube([40, 3.5, board_z]);
        }
    }
}

module lower(t) {

    if (t=="B") {
        // Under-door short foot side for "G"
        hull() {
            translate([0, 0, lower_gap_z-rounding*2.01]) 
                rcube([lower_x, rounding*2.01, rounding*2.01], rounding);
            
            door_gap = door_offset_y - board_gap - lower_gap_z/2;
            translate([0, door_offset_y-board_gap-door_gap, 0])
                rcube([lower_x, board_gap+door_gap, lower_gap_z], rounding);
        }
    }

    translate([0, door_offset_y-board_gap+(board_y+board_gap)*0, 0]) {
        if (t=="B") {
            // short top end stop for half foot
            translate([0, board_gap-door_tab_width, 0])
                rcube([lower_x, door_tab_width, lower_z+door_tab_height], rounding);
        }
        if (t=="A") {
            // full height spacer
            rcube([lower_x, board_gap, lower_z+tab_height], rounding);
        }
        if (t=="C") {
            // extra top only spacer
            width = rounding*2.01;
            raised = lower_z - 0.1;
            translate([0, rounding*2.5, raised])
                rcube([lower_x, width, lower_z+tab_height-raised], rounding);
        }

        hyp=1/2 * (
            sqrt(pow(lower_x, 2)*2) +
            2*(sqrt(pow(rounding, 2)*2)-rounding)
        ) + rounding/5.83;
        
        screw_offset = 0.65;
        base_extra = 0.15;
        translate([0, board_gap/2, 0])  {
            difference() {
                hull() for (side=[-1, 1]) {
                    if (t=="B" && side == -1) {
                        // half length no stop
                        rcube([lower_x, board_gap/2, lower_z], rounding);
                    } else if (t=="D" && side == -1) {
                        rcube([lower_x, board_gap, lower_z], rounding);
                    } else if (t=="C" && side == -1) {
                        hull() {
                            translate([0, 0, lower_z-rounding*2.01])
                                rcube([lower_x, rounding*2.01, rounding*2.01], rounding);
                            translate([0, lower_z-rounding*2.01, 0])
                                rcube([lower_x, rounding*2.01, rounding*2.01], rounding);
                        }
                    } else {
                        translate([lower_x/2, side*(screw_offset+base_extra), 0]) {
                            if (round) {
                                rcylinder(d=lower_x, h=lower_z, rr=rounding);
                            } else {
                                rotate([0, 0, 45])
                                    translate([-hyp/2, -hyp/2, 0])
                                        rcube([hyp, hyp, lower_z], rounding);
                            }
                        } 
                    }
                }
                for (side=[-1, 1]) {
                    translate([lower_x/2, side*screw_offset, 0]) {
                        rotate([180, 0, 0])
                            scale([1/25.4, 1/25.4, 1/25.4])
                                screw();
                    }
                    translate([lower_x/2, side*(screw_offset+base_extra), lower_z-connector_thick]) {
                        difference() {
                            translate([-lower_x/2, -lower_x/2, 0])
                                cube([lower_x, lower_x, connector_thick]);
                            cylinder(d=connector_hole_d, h=connector_thick);
                        }
                    }
                }
            }
        }
    }
}

module connector() {
    difference() {
        hull() {
            translate([connector_width/2, connector_width/2, 0])
                rcylinder(d=connector_width-connector_fudge, h=connector_thick, rr=rounding);
            translate([connector_length - connector_width/2, connector_width/2, 0])
                rcylinder(d=connector_width-connector_fudge, h=connector_thick, rr=rounding);
        }
        translate([connector_width/2, connector_width/2, 0])
            rcylinder_hole(d=connector_hole_d+connector_fudge, h=connector_thick, rr=rounding);
        translate([connector_length - connector_width/2, connector_width/2, 0])
            rcylinder_hole(d=connector_hole_d+connector_fudge, h=connector_thick, rr=rounding);
    }
}


if ($preview) {
    // put a frame around the edge of the bed
    max=200;
    width=0.4*2;
    height=0.31;
    cube([max, width, height]);
    translate([0, max-width, 0])
        cube([max, width, height]);
    cube([width, max, height]);
    translate([max-width, 0, 0])
        cube([width, max, height]);
}

scale([25.4, 25.4, 25.4]) {
    /*
    for (y=[0:2]) {
        translate([0, 2.61*y-0.1, 0])
        for (x=[0:6]) {
            translate([0.08+x*1.1, 1, 0])
                lower(chr(x>2 ? 65 : 67-y));
        }
    }
    */
    for (y=[0:6]) {
        translate([0, 0.15+y*1.1, 0]) {
            connector();
            translate([4.001, 0, 0])
                connector();
        }
    }
}
