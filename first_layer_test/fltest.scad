$fn=$preview ? 16 : 64;

nozzle_width = 0.4; // should match slicer setting
bed_x = 220;
bed_y = 220;
first_layer_thick = 0.3; // should match slicer setting 
thick = first_layer_thick + 0.01;  // note: at exactly 0.3 slic3r core dumps
layer_thick = 0.1; // currently not used

corners = 10; // number of squish test L's in the corners

bed_offset = 20; // don't go all the way to the edge of the bed

size_x = bed_x - bed_offset*2;
size_y = bed_y - bed_offset*2;
line_width = 2*nozzle_width;

module test_pattern() {
    for (i=[0:corners]) {
        difference() {
            offset=pow(i/5,2);
            echo(offset);
            r=line_width*2+line_width*i+offset;
            cube([r, r, thick]);
            cube([r-line_width, r-line_width, thick]);
        }
    }
/*
    acute=2;
    angle_offset=corners*line_width+3*line_width+pow(corners/5,2);
    for (r=[0, 0+acute, 90-acute, 90])
        rotate([0, 0, r])
            mirror([0, r>45 ? 1 : 0, 0])
                translate([angle_offset, 0, 0])
                    cube([size_x/2 - angle_offset, line_width, thick]);
    */
}

for (side_x = [0:1]) {
    for (side_y = [0:1]) {
        translate([side_x*size_x, side_y*size_y, 0]) {
            mirror([side_x, side_y, 0]) {
                test_pattern();
            }
        }
    }
}





/* vim: set ts=4 sw=4 sts=4 et : */
