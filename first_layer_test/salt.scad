$fn=$preview ? 16 : 64;

/*
 * Squish and Level Test (SaLT)
 * 
 * Generates 4 thin outlined rectangles that extend
 * to the corners of the bed, with the gap between
 * the lines designed to be the same width as the
 * thickness of the lines.  When printed, the first
 * layer is "squished" out wider than it should be,
 * which is easily evident from the reduced gap in
 * between the lines.  This provides a visual clue
 * of the bed not being level, where the gap at one
 * corner is different than others, but also allows
 * height adjustments to reduce squish to the bare
 * minimum, which improves print quality.
*/

first_layer_thick = 0.3; // should match slicer setting 

nozzle_width = 0.4; // should match slicer setting

bed_x = 220; // actual bed dimension
bed_y = 220; // actual bed imension

margin = 20; // reduce size for easier printing 

thick = first_layer_thick + 0.01;  // note: at exactly 0.3 slic3r core dumps


size_x = bed_x - margin*2;
size_y = bed_y - margin*2;
line_width = 2*nozzle_width;

function hypo(a, b) = sqrt(a*a + b*b);

size_h = hypo(size_x/2, size_y/2) - 1.5*line_width;

module box(length) {
    for (y = [0, 2])
        translate([0, y*line_width, 0])
            cube([length, line_width, thick]);
    for (x = [0, length-line_width])
        translate([x, 0, 0])
            cube([line_width, 3*line_width, thick]);
}

module pattern() {
    translate([size_h/2, -1.5*line_width, 0])
        box(size_h/2);
    translate([0, -0.5*line_width, 0])
        cube([size_h/2, line_width, thick]);
}


translate([size_x/2, size_y/2, 0])
    for (r = [45:90:360])
        rotate([0, 0, r])
            pattern();
        
/* vim: set ts=4 sw=4 sts=4 et : */
